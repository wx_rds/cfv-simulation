create  or replace table akelly.campaign_ecom_before  as
select a.* from (SELECT
t.crn, t.campaign_code,t.offer_desc, t.offer_type, t.reward_currency_type ,t.activation_start_date,
DATE_DIFF(t.offer_end_date ,t.activation_start_date,day) as RUN_LENGTH,
b.ref_dt, b.pred_bin as pred_bin_before ,
b.adjusted_score_spend as score_spend_before,
case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_before,
ROW_NUMBER() OVER (PARTITION BY b.crn,t.campaign_code ORDER BY b.ref_dt DESC) dest_rank 
    from akelly.campaign_test0  as t join 
     score.cfv_ecom_segments as b
     on t.crn=b.crn AND
      b.ref_dt between date_sub(t.activation_start_date, interval 1 month) AND t.activation_start_date
)  a
     where a.dest_rank=1;

 



create  or replace table akelly.campaign_ecom_after  as
select a.* from (SELECT
t.crn, t.campaign_code,t.offer_end_date,b.ref_dt, b.pred_bin as pred_bin_after ,
b.adjusted_score_spend as score_spend_after,case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_after,
ROW_NUMBER() OVER (PARTITION BY b.crn ,t.campaign_code ORDER BY b.ref_dt DESC) dest_rank 
    from akelly.campaign_test0 as t join 
     score.cfv_ecom_segments as b
     on t.crn=b.crn AND 
      b.ref_dt between  t.offer_end_date AND DATE_ADD(t.offer_end_date, interval 1 month) 
)  a
     where a.dest_rank=1 ;



create  or replace table akelly.campaign_ecom_after  as
SELECT
t.crn, t.campaign_code,t.offer_end_date,b.ref_dt, b.pred_bin as pred_bin_after ,
b.adjusted_score_spend as score_spend_after,case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_after,
ROW_NUMBER() OVER (PARTITION BY b.crn,t.campaign_code ORDER BY b.ref_dt DESC) dest_rank 
    from akelly.campaign_test0 as t join 
     score.cfv_ecom_segments as b
     on t.crn=b.crn AND 
      b.ref_dt between  t.offer_end_date AND DATE_ADD(t.offer_end_date, interval 1 month) ;




create  or replace table akelly.campaign_ecom_before_after  as
  SELECT b.crn,b.offer_desc,b.offer_type,b.reward_currency_type,b.campaign_code,b.RUN_LENGTH,
        CASE when c.pred_rank_after>b.pred_rank_before then 1 else 0 end as cav_conversion,
        CASE when c.score_spend_after>b.score_spend_before then 1 else 0 end as spend_increase_conversion,
        CASE when c.score_spend_after>b.score_spend_before*1.05 then 1 else 0 end as spend_increase_conversion_5pcent,
        CASE when c.score_spend_after>b.score_spend_before*1.1 then 1 else 0 end as spend_increase_conversion_10pcent,
        (c.score_spend_after/b.score_spend_before)-1 as spend_change_pcent,
        b.score_spend_before,c.score_spend_after,
        b.pred_bin_before,c.pred_bin_after
        from 
        akelly.campaign_ecom_before as b left  join 
        akelly.campaign_ecom_after as c on 
        (b.crn=c.crn and b.campaign_code=c.campaign_code );



create  or replace table akelly.campaign_newbin as  
select * EXCEPT (pred_bin_after,cav_conversion), 
case when pred_bin_before='high' and pred_bin_after='high' and score_spend_after-score_spend_before>=2000 then 'extra high' else
pred_bin_after end as pred_bin_after,
case when pred_bin_before='high' and pred_bin_after='high' and score_spend_after-score_spend_before>=2000 then 1 else
cav_conversion end as cav_conversion
from  akelly.campaign_ecom_before_after ; 

 
create  or replace table akelly.campaign_ecom_activated  as select 
aa.*  
,count(case when b.activation_ts IS NOT NULL and a.test_exec_ind != 'Y' then 1  end) as activated
FROM akelly.campaign_newbin  as aa INNER JOIN
        wx-bq-poc.loyalty.campaign_exec a on aa.campaign_code=a.cmpgn_code
INNER JOIN  wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation b 
on          a.cmpgn_exec_id = b.cmpgn_exec_id  and aa.crn=b.crn
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;



 

create  or replace table akelly.campaign_ecom_aggregate as
select   
b.offer_desc,b.offer_type
,b.reward_currency_type
,AVG(b.RUN_LENGTH) as avg_run_length
, count(distinct(b.campaign_code)) as campaigns
,count(*) as customers
, count(distinct(b.crn)) as unique_customers
, count( case when b.activated !=0 then 1 end) as no_of_activated
, count( case when b.pred_bin_after is NULL then 1 end)/count(*)  as campaign_churn
, sum(b.cav_conversion) as cav_conversion_sum
, sum(case when b.activated !=0 then b.cav_conversion end) as cav_conversion_activated_sum
, sum(b.cav_conversion)/count(*) as cav_conversion_rate
, sum(case when b.activated !=0 then b.cav_conversion end)/count(*) as cav_conversion_activated_rate
, sum(case when b.activated !=0 then b.cav_conversion end)/count( case when b.activated !=0 then 1 end) as cav_conversion_activated_rate_from_activated

, sum(  b.spend_increase_conversion) as spend_increase_conversion_sum
, sum(case when b.activated !=0 then b.spend_increase_conversion end) as spend_increase_conversion_activated_sum

, sum( b.spend_increase_conversion)/count(*) as spend_increase_conversion_rate
, sum(case when b.activated !=0 then  b.spend_increase_conversion end)/count(*) as spend_increase_conversion_activated_rate


, sum( b.spend_increase_conversion_5pcent) as spend_increase_conversion_5pcent_sum
, sum(case when b.activated !=0 then   b.spend_increase_conversion_5pcent end) as spend_increase_conversion_activated_5pcent_sum


, sum(b.spend_increase_conversion_5pcent)/count(*) as spend_increase_conversion_5pcent_rate
, sum(case when b.activated !=0 then  b.spend_increase_conversion_5pcent end)/count(*) as spend_increase_conversion_activated_5pcent_rate


, sum( b.spend_increase_conversion_10pcent) as spend_increase_conversion_10pcent_sum
, sum(case when b.activated !=0 then   b.spend_increase_conversion_10pcent end) as spend_increase_conversion_activated_10pcent_sum


, sum(b.spend_increase_conversion_10pcent)/count(*) as spend_increase_conversion_10pcent_rate
, sum(case when b.activated !=0 then  b.spend_increase_conversion_10pcent end)/count(*) as spend_increase_conversion_activated_10pcent_rate
,AVG(case when b.activated =0 then   b.spend_change_pcent end) as after_spend_pcent_not_activated_change
,AVG(case when b.activated !=0 then   b.spend_change_pcent end) as after_spend_pcent_activated_change
, AVG(case when b.activated !=0 then  b.score_spend_before end) as avg_spend_before_activated
, AVG(case when b.activated !=0 then  b.score_spend_after end) as avg_spend_after_activated

, AVG(case when b.activated !=0 and cav_conversion>0 then  b.score_spend_before end) as avg_spend_before_uplifted
, AVG(case when b.activated !=0 and cav_conversion>0 then  b.score_spend_after end) as avg_spend_after_uplifted

,AVG( b.spend_change_pcent) as after_spend_pcent_change
, AVG(b.score_spend_before) as avg_spend_before
, AVG(b.score_spend_after) as avg_spend_after
,STDDEV(case when activated !=0 then 1 else 0 end) as activated_std
,STDDEV(case when activated !=0 and cav_conversion=1 then 1 else 0 end) as activated_cav_conversion_std
,STDDEV(case when activated !=0 and spend_increase_conversion_5pcent=1 then 1 else 0 end) as activated_spend_increase_conversion_5pcent_std
,STDDEV(case when activated !=0 and spend_increase_conversion_10pcent=1 then 1 else 0 end) as activated_spend_increase_conversion_10pcent_std
from akelly.campaign_ecom_activated b group by 1,2,3;