

create or replace table akelly.campaign_ref_dt  as 
select distinct ref_dt, campaign_code,offer_desc, offer_type, reward_currency_type ,activation_start_date,RUN_LENGTH,
from akelly.campaign_test2  
group by 1,2,3,4,5,6,7;
 
create or replace table akelly.campaign_ref_dt_crn  as 
select crn,ref_dt, campaign_code ,  activation_start_date
from akelly.campaign_test2  
group by 1,2,3,4;

 

 

create  or replace table akelly.campaign_control_before  as
select 
a.*
from (SELECT DISTINCT b.crn, d.campaign_code,d.offer_desc, d.offer_type, d.reward_currency_type ,d.activation_start_date,d.RUN_LENGTH,
b.ref_dt, b.pred_bin as pred_bin_before ,
b.adjusted_score_spend as score_spend_before,
case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_before,
ROW_NUMBER() OVER (PARTITION BY b.pred_bin,b.ref_dt ORDER BY b.ref_dt DESC) bin_rank 
from akelly.campaign_ref_dt d join score.cfv_instore_segments b   on 
b.ref_dt=d.ref_dt left join akelly.campaign_ref_dt_crn c on b.ref_dt=c.ref_dt and   c.crn is Null where  b.pred_bin !='zero')  a
     where a.bin_rank<=500  and campaign_code is not null
     group by 1,2,3,4,5,6,7,8,9,10,11,12;

#697904783




 
create  or replace table akelly.campaign_control_after as
SELECT DISTINCT b.crn,c.campaign_code,c.offer_desc, c.offer_type, c.reward_currency_type ,c.activation_start_date,c.RUN_LENGTH,
b.ref_dt, 
b.pred_bin as pred_bin_after ,
b.adjusted_score_spend as score_spend_after,
case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_after
from score.cfv_instore_segments b  join akelly.campaign_control_before c on 
b.crn=c.crn and  b.ref_dt between  DATE_ADD(c.ref_dt, interval 2 month)  AND DATE_ADD(c.ref_dt, interval 3 month)
group by 1,2,3,4,5,6,7,8,9,10,11;






create  or replace table akelly.campaign_control_after_withcamp  as select 
a.*,
 t.campaign_code,t.offer_desc, t.offer_type, t.reward_currency_type ,t.activation_start_date,t.RUN_LENGTH
 from akelly.campaign_control_before  as a join akelly.campaign_test2 as t on a.ref_dt=t.ref_dt
 group by 1,2,3,4,5,6,7,8,9,10,11,12;




