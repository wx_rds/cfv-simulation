import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import seaborn as sns
import os
import sys
from google.cloud import bigquery
from google.cloud import storage
import pickle
import matplotlib.ticker as mtick



os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
#from pandas_profiling import ProfileReport
pd.options.display.float_format = '{:.4f}'.format

#spreadsheet location https://docs.google.com/spreadsheets/d/17ZsLzPjtbOIQmqhaWn62roa-kcuG41FgnGBYfC_qJB4/edit#gid=0
project_id = 'gcp-wow-rwds-ai-cfv-dev'
 
def get_conversion_rate(expected, stdev):
    conversion_rate = max(expected + np.random.normal()*(stdev), #stdev, 
                          0.01)
    if conversion_rate>=1:
        return 0.99
    else:
        return conversion_rate

# Function for calculating the results of a marketing campaign
def run_campaign(conversion_rate, targeted_customers): #=None,spend=None, activated_cost=None):
    return np.random.binomial(targeted_customers, conversion_rate)


    
# Function that models the progression of a cohort over time
def simulate_cohort(cohort_size, churn_rate, avg_yr_pred, yrs=1,before=False):
    customers_left = []
    spending = []
    profit = []
    for i in range(yrs):
        if before==True:
            spending.append(cohort_size*avg_yr_pred)
            return cohort_size, spending
        else:
      
            for customer in range(cohort_size):
                # Assume cancels happen at the start of the year 
                # (for simplicity)
                
                churn_random_num = np.random.random()
                # Generate a random number between 0 and 1, if less 
                # than churn_rate then customer has churned and we 
                # subtract 1 from cohort_size
                if churn_random_num <= churn_rate:
                    cohort_size += -1
                # Calculate and record cohort's data
            customers_left.append(cohort_size)
            spending.append(cohort_size*avg_yr_pred)

            return cohort_size, spending


def simiulation_step1(campaigns,activated_cost,offer,runs=1000,control=False):
    test=campaigns_ecom[campaigns_ecom.offer_desc==offer]

    chosen_campaign=campaigns[campaigns.offer_desc==offer]
    targeted_customers=int(chosen_campaign.customers/chosen_campaign.campaigns)
    conversion_rate_activated_expected =chosen_campaign.cav_conversion_rate.iloc[0]
    conversion_rate_activated_std=chosen_campaign.activated_std.iloc[0]
    conversion_rate_uplifted_expected =chosen_campaign.cav_conversion_activated_rate_from_activated.iloc[0]
    conversion_rate_uplifted_std=chosen_campaign.activated_cav_conversion_std.iloc[0]



    simulate=pd.DataFrame(columns=['CAC','cohort_size_activated','cohort_size_uplift','Pcent_uplift','Pcent_uplift_oftotal','Pcent_activated','customers_not_activated_before','customers_not_activated_after','customers_activated_before','customers_activated_after','customers_uplifted_before','customers_uplifted_after','spending_not_activated_before', 'spending_not_activated_after','spending_before_activated', 'spending_after_activated', 'spending_before_uplifted', 'spending_after_uplifted','cohort_uplifted_value','spread' ,'activated_conversion_rate','activated_uplift_conversion_rate'])


    for i in range(runs):
        if i==1:
            print('chosen_campaign: ',offer )
            print('targeted_customers: ',targeted_customers )
            print('conversion_rate_activated_expected: ',conversion_rate_activated_expected )
            print('conversion_rate_activated_std: ',conversion_rate_activated_std )
            print('conversion_rate_uplifted_expected: ',conversion_rate_uplifted_expected )
            print('conversion_rate_uplifted_std: ',conversion_rate_uplifted_std )
        # Run marketing campaign sim
       	
       
        activated_conversion_rate = get_conversion_rate(conversion_rate_activated_expected,conversion_rate_activated_std*0.01)
        activated_uplift_conversion_rate = get_conversion_rate(conversion_rate_uplifted_expected, conversion_rate_uplifted_std*0.01)
        cohort_size_activated = max(run_campaign( activated_conversion_rate,targeted_customers=targeted_customers),1)
        if cohort_size_activated>=targeted_customers:
             cohort_size_activated=targeted_customers-1
        else:
            cohort_size_activated=cohort_size_activated
        cohort_size_uplift = max(run_campaign( activated_uplift_conversion_rate,targeted_customers=cohort_size_activated),1)
        #CAC = spend/cohort_size
        #cost of campaign
        CAC = int(activated_cost*cohort_size_activated)
        Pcent_uplift_oftotal=cohort_size_uplift/targeted_customers
        Pcent_uplift=cohort_size_uplift/cohort_size_activated
        Pcent_activated=cohort_size_activated/targeted_customers
        cohort_size_not_activated=targeted_customers-cohort_size_activated
        # Run the function
        
        
        customers_not_activated_before, spending_not_activated_before =simulate_cohort(cohort_size_not_activated, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_not_activated.iloc[0], yrs=1,before=True)

        customers_not_activated_after, spending_not_activated_after =simulate_cohort(cohort_size_not_activated, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_not_activated.iloc[0], yrs=1)
        
        
        customers_activated_before, spending_before_activated =simulate_cohort(cohort_size_activated, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_activated.iloc[0], yrs=1,before=True)

        customers_activated_after, spending_after_activated =simulate_cohort(cohort_size_activated, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_activated.iloc[0], yrs=1)


        customers_uplifted_before, spending_before_uplifted =simulate_cohort(cohort_size_uplift, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_uplifted.iloc[0], yrs=1,before=True)

        customers_uplifted_after, spending_after_uplifted =simulate_cohort(cohort_size_uplift, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_uplifted.iloc[0], yrs=1)

        cohort_uplifted_value = sum(spending_after_uplifted)-sum(spending_before_uplifted)
        spread=int(cohort_uplifted_value - CAC)
        temp=pd.DataFrame([[CAC,cohort_size_activated,cohort_size_uplift,Pcent_uplift,Pcent_uplift_oftotal,\
                                   float(Pcent_activated),customers_not_activated_before,customers_not_activated_after,customers_activated_before,customers_activated_after,\
                                       customers_uplifted_before,customers_uplifted_after,spending_not_activated_before[0],\
                                       spending_not_activated_after[0],spending_before_activated[0],\
                                       spending_after_activated[0], spending_before_uplifted[0],spending_after_uplifted[0],\
                                       cohort_uplifted_value,spread,activated_conversion_rate,activated_uplift_conversion_rate]],columns=['CAC','cohort_size_activated','cohort_size_uplift','Pcent_uplift','Pcent_uplift_oftotal','Pcent_activated','customers_not_activated_before','customers_not_activated_after','customers_activated_before','customers_activated_after','customers_uplifted_before','customers_uplifted_after','spending_not_activated_before', 'spending_not_activated_after','spending_before_activated', 'spending_after_activated', 'spending_before_uplifted', 'spending_after_uplifted','cohort_uplifted_value','spread','activated_conversion_rate','activated_uplift_conversion_rate'])


        simulate=simulate.append(temp)
    simulate=simulate.reset_index()
    return simulate
   
activated_cost=5
offer='SPEND $60 GET 1500 PTS'
runs=1500
simulate=simiulation_step1(campaigns,activated_cost,offer,runs)
simulate_ecom=simiulation_step1(campaigns_ecom,activated_cost,offer,runs)   
campaigns_control=campaigns_control.fillna(0)
simulate_control=simiulation_step1(campaigns_control,activated_cost,offer,runs,control=True)   
 


simulate.to_feather('simulate2.feather')
simulate_ecom.to_feather('simulate_ecom2.feather')
simulate_control.to_feather('simulate_control2.feather')

simulate=pd.read_feather('simulate2.feather')
simulate_ecom=pd.read_feather('simulate_ecom2.feather')
simulate_control=pd.read_feather('simulate_control2.feather')


plot_df = simulate #[results_df['CAC']<=1000]?
 

simulate.columns

greens=['forestgreen','limegreen','darkgreen','green','lime','seagreen', 'mediumseagreen','springgreen','lightgreen','palegreen']


print('The mean instore activation rate is: ',  "{0:.2%}".format(simulate.activated_conversion_rate.mean()) 	)
print('The mean instore uplifted CAV rate (from activated samples) is: ',  "{0:.2%}".format(simulate.activated_uplift_conversion_rate.mean()) 	)
print('The mean instore activation rate is: ',  "{0:.2%}".format(simulate_ecom.activated_conversion_rate.mean()) 	)
print('The mean instore uplifted CAV rate (from activated samples) is: ',  "{0:.2%}".format(simulate_ecom.activated_uplift_conversion_rate.mean()) 	)


 
fig, ax = plt.subplots(1,2,figsize=(10,5)) 
 
sns.barplot(
    x=["Instore","Ecom"], 
    y=[simulate.activated_conversion_rate.mean(),simulate_ecom.activated_conversion_rate.mean() ], color=greens[2],ax=ax[0]);
 
sns.barplot(
    x=["Instore","Ecom"], 
    y=[simulate.activated_uplift_conversion_rate.mean(), simulate_ecom.activated_uplift_conversion_rate.mean()], color=greens[2],ax=ax[1]);
  

ax[0].set_ylabel('Density', fontsize=10)
ax[0].set_xlabel('Activation rate', fontsize=10)
ax[0].yaxis.set_major_formatter(mtick.PercentFormatter())


ax[1].set_ylabel('Density', fontsize=10)
ax[1].set_xlabel('Uplift rate', fontsize=10)
ax[1].yaxis.set_major_formatter(mtick.PercentFormatter())

fig.suptitle("Instore vs. Ecom activation rates", fontsize=16)

fig.tight_layout()

fig.show()

fig, ax = plt.subplots(figsize=(9,6))

sns.distplot(simulate['customers_uplifted_after'],color= greens[2], kde=False, bins=20)
sns.distplot(simulate_ecom['customers_uplifted_after'],color= greens[3], kde=False, bins=20)
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.ticklabel_format(style='plain', axis='y')
plt.ticklabel_format(style='plain', axis='x')
plt.title('Instore activated and CAV uplifed (subset of activated) - Instore and Ecom', fontsize=20)
fig.legend(labels=['Instore','Ecom'], loc='lower center', borderaxespad=-0.7)

plt.tight_layout()

plt.show()


fig, ax = plt.subplots(figsize=(9,6))

sns.distplot(simulate['customers_activated_after'],color= greens[2], kde=False, bins=20)
sns.distplot(simulate_ecom['customers_activated_after'],color= greens[3], kde=False, bins=20)
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.ticklabel_format(style='plain', axis='y')
plt.ticklabel_format(style='plain', axis='x')
plt.title('Activated Campaign- Instore and Ecom', fontsize=20)
fig.legend(labels=['Instore','Ecom'], loc='lower center', borderaxespad=-0.7)

plt.tight_layout()

plt.show()
 

fig, ax = plt.subplots(1,2,figsize=(10,5))
#sns.kdeplot(plot_df['spending_before_activated'], color= greens[0], shade=True, Label='before_activated')
#sns.kdeplot(plot_df['spending_after_activated'], color=greens[1], shade=True, Label='after_activated')
sns.kdeplot(simulate['spending_before_uplifted'], color= greens[2], shade=True, Label='online',ax=ax[0])
sns.kdeplot(simulate['spending_after_uplifted'], color=greens[3], shade=True, Label='instore',ax=ax[0])

sns.kdeplot(simulate_ecom['spending_before_uplifted'], color= greens[2], shade=True, Label='online',ax=ax[1])
sns.kdeplot(simulate_ecom['spending_after_uplifted'], color=greens[3], shade=True, Label='instore',ax=ax[1])
# Setting the X and Y Label

ax[0].set_ylabel('Density', fontsize=10)
ax[0].set_xlabel('Instore', fontsize=10)
ax[0].ticklabel_format(style='plain', axis='y')
ax[0].ticklabel_format(style='plain', axis='x')


ax[1].set_ylabel('Density', fontsize=10)
ax[1].set_xlabel('Ecom', fontsize=10)
ax[1].ticklabel_format(style='plain', axis='y')
ax[1].ticklabel_format(style='plain', axis='x')
fig.suptitle("Uplifted cohort - spending before vs. after campaign", fontsize=16)
ax[0].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)
ax[1].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)

fig.tight_layout()

fig.show()


fig, ax = plt.subplots(1,2,figsize=(10,5))
#sns.kdeplot(plot_df['spending_before_activated'], color= greens[0], shade=True, Label='before_activated')
#sns.kdeplot(plot_df['spending_after_activated'], color=greens[1], shade=True, Label='after_activated')
sns.kdeplot(simulate['spending_before_activated'], color= greens[2], shade=True, Label='online',ax=ax[0])
sns.kdeplot(simulate['spending_after_activated'], color=greens[3], shade=True, Label='instore',ax=ax[0])

sns.kdeplot(simulate_ecom['spending_before_activated'], color= greens[2], shade=True, Label='online',ax=ax[1])
sns.kdeplot(simulate_ecom['spending_after_activated'], color=greens[3], shade=True, Label='instore',ax=ax[1])
# Setting the X and Y Label

ax[0].set_ylabel('Density', fontsize=10)
ax[0].set_xlabel('Instore', fontsize=10)
ax[0].ticklabel_format(style='plain', axis='y')
ax[0].ticklabel_format(style='plain', axis='x')


ax[1].set_ylabel('Density', fontsize=10)
ax[1].set_xlabel('Ecom', fontsize=10)
ax[1].ticklabel_format(style='plain', axis='y')
ax[1].ticklabel_format(style='plain', axis='x')
fig.suptitle("Activated cohort - spending before vs. after campaign", fontsize=16)
ax[0].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)
ax[1].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)

fig.tight_layout()

fig.show()

fig, ax = plt.subplots(1,2,figsize=(10,5))
#sns.kdeplot(simulate['cohort_uplifted_value'], color=greens[2], shade=True, Label='instore',ax=ax[0])
#sns.kdeplot(simulate_ecom['cohort_uplifted_value'], color=greens[3], shade=True, Label='instore',ax=ax[0])

sns.distplot(simulate['cohort_uplifted_value'],color= greens[2], kde=False, bins=20,ax=ax[0])
sns.distplot(simulate_ecom['cohort_uplifted_value'],color= greens[3], kde=False, bins=20,ax=ax[0])

sns.distplot(simulate['CAC'],color= greens[2], kde=False, bins=20,ax=ax[1])
sns.distplot(simulate_ecom['CAC'],color= greens[3], kde=False, bins=20,ax=ax[1])
ax[0].set_ylabel('Frequency', fontsize=10)
ax[0].set_xlabel('Uplifted spend (after - before)', fontsize=10)
ax[0].ticklabel_format(style='plain', axis='y')
ax[0].ticklabel_format(style='plain', axis='x')


ax[1].set_ylabel('Frequency', fontsize=10)
ax[1].set_xlabel('Cost of campaign', fontsize=10)
ax[1].ticklabel_format(style='plain', axis='y')
ax[1].ticklabel_format(style='plain', axis='x')
fig.suptitle("Expected revenue and cost of campaign", fontsize=16)
ax[0].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)
ax[1].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)

fig.tight_layout()

fig.show()
	


 


fig, ax = plt.subplots(figsize=(9,6))
sns.kdeplot(simulate['spread'], color= greens[2], shade=True, Label='online')
sns.kdeplot(simulate_ecom['spread'], color=greens[3], shade=True, Label='instore')
 
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.axvline(x=0, color='red')
plt.ticklabel_format(style='plain', axis='y')
plt.ticklabel_format(style='plain', axis='x')
plt.title('Uplifted value - cost of campaign', fontsize=20)
fig.legend(labels=['zero','Instore','Ecom'], loc='upper right', borderaxespad=-0.5)

plt.tight_layout()

plt.show()



 
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['cohort_size_activated'],color= greens[2], kde=False, bins=100)
sns.distplot(plot_df['cohort_size_uplift'],color= greens[2], kde=False, bins=100)

#sns.kdeplot(plot_df['cohort_size_activated'], color= greens[2], shade=True, Label='online')

ax.set_xlabel("cohort_size_activated",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.tight_layout()
plt.ticklabel_format(style='plain', axis='y')
plt.ticklabel_format(style='plain', axis='x')
plt.show()




# Histogram for distribution of CLTV-CAC Spread
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['spread'], kde=False, bins=150)
plt.axvline(x=0, color='red')
#plt.xlim(-200, 80)

ax.set_xlabel("CLTV - CAC Spread",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.tight_layout()

plt.savefig(fname='spread_hist', dpi=150)
plt.show()








