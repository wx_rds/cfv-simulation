import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import seaborn as sns
import os
import sys
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
#from pandas_profiling import ProfileReport

import dash
import dash_html_components as html
import dash_core_components as dcc
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
from dash import Dash, dcc, html, Input, Output,State
import dash_bootstrap_components as dbc
import plotly.figure_factory as ff

from plotly.offline import download_plotlyjs, init_notebook_mode, plot
from itertools import cycle
palette = cycle(px.colors.qualitative.Bold)
#palette = cycle(['black', 'grey', 'red', 'blue'])
palette = cycle(px.colors.sequential.PuBu




#spreadsheet location https://docs.google.com/spreadsheets/d/17ZsLzPjtbOIQmqhaWn62roa-kcuG41FgnGBYfC_qJB4/edit#gid=0
project_id = 'gcp-wow-rwds-ai-cfv-dev'

campaigns =pd.read_gbq('select * from akelly.campaign_instore_agg order by 1', project_id=project_id)
#campaigns_ecom =pd.read_gbq('select * from akelly.campaign_ecom_aggregate order by 1', project_id=project_id)
#campaigns_control =pd.read_gbq('select * from akelly.campaign_control_agg order by 1', project_id=project_id)

options=pd.Series(campaigns.offer_desc.values,index=campaigns.cav_conversion_activated_rate).to_dict()

options=pd.Series(campaigns.offer_desc.values)


simulate=pd.read_feather('simulate2.feather')
simulate_ecom=pd.read_feather('simulate_ecom2.feather')

simulate['type']='instore'
simulate_ecom['type']='ecom'

simulate_all=simulate.append(simulate_ecom)
################################3
################testing graphs################3
df = px.data.stocks()
    fig = go.Figure([go.Scatter(x = df['date'], y = df['GOOG'],\
                     line = dict(color = 'firebrick', width = 4), name = 'Google')
                     ])
    fig.update_layout(title = 'Prices over time',
                      xaxis_title = 'Dates',
                      yaxis_title = 'Prices'
                      )

plot(fig)  


colorscales = px.colors.named_colorscales()


    fig = px.histogram(simulate_all, x="customers_uplifted_after",  color="type",
                   marginal="box", # or violin, rug
                   hover_data=simulate_all.columns,
                   color_discrete_sequence=['green','lightgreen'])
    fig.update_layout(title = 'Estimated uplift - instore and ecom',
                      xaxis_title = 'No. of customers',
                      yaxis_title = 'Frequ'
                      )
plot(fig)  



    fig = px.histogram(simulate_all, x="customers_activated_after",  color="type",
                   marginal="box", # or violin, rug
                   hover_data=simulate_all.columns,
                   color_discrete_sequence=['green','lightgreen'])
    fig.update_layout(title = 'Estimated activated - instore and ecom',
                      xaxis_title = 'No. of customers',
                      yaxis_title = 'Frequency'
                      )
plot(fig)  




group_labels = ['Instore uplifted spending before', 'Instore uplifted spending after','Ecom uplifted spending before', 'Ecom uplifted spending after']

x1=simulate_all[simulate_all.type=='instore'].spending_before_uplifted
x2=simulate_all[simulate_all.type=='instore'].spending_after_uplifted
x3=simulate_all[simulate_all.type=='ecom'].spending_before_uplifted
x4=simulate_all[simulate_all.type=='ecom'].spending_after_uplifted

# Create distplot with custom bin_size

hist_data = [x1,x2,x3,x4]
fig = ff.create_distplot(hist_data, group_labels, bin_size=200000)
plot(fig)  




##################################


print('The mean instore activation rate is: ',  "{0:.2%}".format(simulate_all[simulate_all.type=='instore'].activated_conversion_rate.mean()), 	 
 '\n The mean instore uplifted CAV rate (from activated samples) is: ',  "{0:.2%}".format(simulate_all[simulate_all.type=='instore'].activated_uplift_conversion_rate.mean()), 	 
 '\n The mean instore activation rate is: ',  "{0:.2%}".format(simulate_all[simulate_all.type=='ecom'].activated_conversion_rate.mean()) 	, 
 '\n The mean instore uplifted CAV rate (from activated samples) is: ',  "{0:.2%}".format(simulate_all[simulate_all.type=='ecom'].activated_uplift_conversion_rate.mean()) 	)



 

app = dash.Dash(external_stylesheets=[dbc.themes.DARKLY])   #initialising dash app

SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "4rem 1rem 2rem" 
    #"background-color": "#f8f9fa",
}


CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "4rem 1rem 2rem",
}
 
@app.callback(Output('uplift_hist', 'figure'), [Input('submit_input', 'n_clicks')],
                                              [State('dropdown', 'value'),
                                              State('activation_cost', 'value'),
                                              State('runs', 'value')
                                              ])
def showg1(n_clicks,campaign,cost,runs):
    print(n_clicks)
    # Function for creating line chart showing Google stock prices over time 
    fig = px.histogram(simulate_all, x="customers_uplifted_after",  color="type",
                   marginal="box", # or violin, rug
                   hover_data=simulate_all.columns,
                   color_discrete_sequence=['green','lightgreen'])
    fig.update_layout(title = 'Estimated uplift - instore and ecom for '+campaign+' '+ str(cost)+ ' ' +str(runs),
                      xaxis_title = 'No. of customers',
                      yaxis_title = 'Frequ'
                      )
    if n_clicks is None:
        return dash.no_update
    else:
        return fig  



@app.callback(Output('activate_hist', 'figure'), [Input('submit_input', 'n_clicks')],
                                              [State('dropdown', 'value'),
                                              State('activation_cost', 'value'),
                                              State('runs', 'value')
                                              ])
def showg2(n_clicks,campaign,cost,runs):
    print(n_clicks)
    # Function for creating line chart showing Google stock prices over time 
    fig = px.histogram(simulate_all, x="customers_activated_after",  color="type",
                   marginal="box", # or violin, rug
                   hover_data=simulate_all.columns,
                   color_discrete_sequence=['green','lightgreen'])
    fig.update_layout(title = 'Estimated activated - instore and ecom',
                      xaxis_title = 'No. of customers',
                      yaxis_title = 'Frequency'
                      )
    if n_clicks is None:
        return dash.no_update
    else:
        return fig  




@app.callback(Output('activate_uplift_spend', 'figure'), [Input('submit_input', 'n_clicks')],
                                              [State('dropdown', 'value'),
                                              State('activation_cost', 'value'),
                                              State('runs', 'value')
                                              ])
def showg3(n_clicks,campaign,cost,runs):

    # Function for creating line chart showing Google stock prices over time 

    group_labels = ['Instore uplifted spending before', 'Instore uplifted spending after','Ecom uplifted spending before', 'Ecom uplifted spending after']
    
    x1=simulate_all[simulate_all.type=='instore'].spending_before_uplifted
    x2=simulate_all[simulate_all.type=='instore'].spending_after_uplifted
    x3=simulate_all[simulate_all.type=='ecom'].spending_before_uplifted
    x4=simulate_all[simulate_all.type=='ecom'].spending_after_uplifted
    
    # Create distplot with custom bin_size
    
    hist_data = [x1,x2,x3,x4]
    fig = ff.create_distplot(hist_data, group_labels, bin_size=200000)
    
    fig.update_layout(title = 'Estimated spending before and after campaign for uplifted cohort',
                      xaxis_title = 'No. of customers',
                      yaxis_title = 'Frequency'
                      )
    if n_clicks is None:
        return dash.no_update
    else:
        return fig  
    
    
    
@app.callback(Output('uplift_raw_hist', 'figure'), [Input('submit_input', 'n_clicks')],
                                              [State('dropdown', 'value'),
                                              State('activation_cost', 'value'),
                                              State('runs', 'value')
                                              ])
def showg4(n_clicks,campaign,cost,runs):
    print(n_clicks)
    # Function for creating line chart showing Google stock prices over time 
    fig = px.histogram(simulate_all, x="cohort_uplifted_value",  color="type",
                   marginal="box", # or violin, rug
                   hover_data=simulate_all.columns,
                   color_discrete_sequence=['green','lightgreen'])
    fig.update_layout(title = 'Expected revenue from uplift (before spend-after spend)' ,
                      xaxis_title = 'Revenue',
                      yaxis_title = 'Frequ'
                      )
    if n_clicks is None:
        return dash.no_update
    else:
        return fig  


@app.callback(Output('cac', 'figure'), [Input('submit_input', 'n_clicks')],
                                              [State('dropdown', 'value'),
                                              State('activation_cost', 'value'),
                                              State('runs', 'value')
                                              ])
def showg5(n_clicks,campaign,cost,runs):
    print(n_clicks)
    # Function for creating line chart showing Google stock prices over time 
    fig = px.histogram(simulate_all, x="CAC",  color="type",
                   marginal="box", # or violin, rug
                   hover_data=simulate_all.columns,
                   color_discrete_sequence=['green','lightgreen'])
    fig.update_layout(title = 'Estimated Cost of campaign',
                      xaxis_title = 'Cost',
                      yaxis_title = 'Frequency'
                      )
    if n_clicks is None:
        return dash.no_update
    else:
        return fig  
    
    
@app.callback(Output('spread', 'figure'), [Input('submit_input', 'n_clicks')],
                                              [State('dropdown', 'value'),
                                              State('activation_cost', 'value'),
                                              State('runs', 'value')
                                              ])
def showg5(n_clicks,campaign,cost,runs):
    print(n_clicks)
    # Function for creating line chart showing Google stock prices over time 
    fig = px.histogram(simulate_all, x="spread",  color="type",
                   marginal="box", # or violin, rug
                   hover_data=simulate_all.columns,
                   color_discrete_sequence=['green','lightgreen'])
    fig.update_layout(title = 'Spread - uplift minus cost of campaign',
                      xaxis_title = 'Spread $',
                      yaxis_title = 'Frequency'
                      )
    if n_clicks is None:
        return dash.no_update
    else:
        return fig  
 
@app.callback(Output('activation-uplift-output', 'children'), [Input('submit_input', 'n_clicks')])

def update_activation_uplift_output(n_clicks):
    text=('The mean instore activation rate is: ',  "{0:.2%}".format(simulate_all[simulate_all.type=='instore'].activated_conversion_rate.mean()), 	 
 '\n The mean instore uplifted CAV rate (from activated samples) is: ',  "{0:.2%}".format(simulate_all[simulate_all.type=='instore'].activated_uplift_conversion_rate.mean()), 	 
 '\n The mean instore activation rate is: ',  "{0:.2%}".format(simulate_all[simulate_all.type=='ecom'].activated_conversion_rate.mean()) 	, 
 '\n The mean instore uplifted CAV rate (from activated samples) is: ',  "{0:.2%}".format(simulate_all[simulate_all.type=='ecom'].activated_uplift_conversion_rate.mean()) )
    
    if n_clicks is None:
        return dash.no_update
    else:
        return text  



sidebar = html.Div(id="sidebar", children=
    [
    html.P('Select campaign to simiulate'),
    dcc.Dropdown( id = 'dropdown',
                    options = options,
                    value = 'SPEND $60 GET 1500 PTS' ,
                    style={"width": "95%",'color': 'black'}
                    ),
    html.Br(),
    html.P('Enter activation cost per customer'),
    dcc.Input(
        id = 'activation_cost',
        placeholder='Activation Cost',
        type = 'number',
        value = 5,
    ),
    html.Br(),
    html.P('Enter No. of simulations'),
    dcc.Input(
        id = 'runs',
        placeholder='Number of simulationss',
        type = 'number',
        value = 100,
    ),
    html.Br(),
    html.Button(id='submit_input', n_clicks=0, children='Simiulate'),
    html.Br()
    ]
    ,style=SIDEBAR_STYLE
)

content = html.Div(id="content", children=
    [html.H1(id = 'H1', children = 'CAV simulation tool', style = {'textAlign':'center',\
                                            'marginTop':40,'marginBottom':40}),
    html.Div(id='activation-uplift-output', style={'whiteSpace': 'pre-line'}),
    dcc.Graph(id = 'uplift_hist'),
    dcc.Graph(id = 'activate_hist'),
    dcc.Graph(id = 'activate_uplift_spend'),
    dcc.Graph(id = 'uplift_raw_hist'),
    dcc.Graph(id = 'cac'),
    dcc.Graph(id = 'spread')
    
    

    ],style=CONTENT_STYLE
                   
                   )

app.layout = html.Div([ sidebar, content])



if __name__ == '__main__': 
    app.run_server()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


app.layout = html.Div(id = 'parent'
    html.H1(id = 'H1', children = 'CAV simulation tool', style = {'textAlign':'center',\
                                            'marginTop':40,'marginBottom':40}),

    dcc.Dropdown( id = 'dropdown',
                    options = options,
                    value = 'SPEND $60 GET 1500 PTS' ,
                    style={"width": "40%"}
                    ),
    html.Br(),
    dcc.Input(
        id = 'activation_cost',
        placeholder='Activation Cost',
        type = 'number',
        value = 5,
    ),
    html.Br(),
    dcc.Input(
        id = 'runs',
        placeholder='Number of simulationss',
        type = 'number',
        value = 100,
    ),
    html.Br(),
    html.Button(id='submit_input', n_clicks=0, children='Simiulate'),
    html.Br(), 
    ])
    
    
    
    
    
    
    
    