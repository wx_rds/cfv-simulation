

SELECT * from score.cfv_instore_segments 
 where 
 crn='1000000000001588180' and
 ref_dt between date_sub('2021-07-17', interval 1 month) AND '2021-07-17';

select * from 
`gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v 
where PrimaryCustomerRegistrationNumber= '1000000000001588180' ;


select offer_desc,offer_type
,reward_currency_type ,count(distinct(campaign_code)) as campaigns, count(distinct(crn)) as crns, count(*) as cnt
from wx-bq-poc.loyalty.et_targeted_customer_offer  
where campaign_start_date>='2021-06-01' and offer_end_date<'2021-12-01' group by 1,2,3;    




create  or replace table akelly.campaign_test0 as
select DISTINCT
   a.crn ,a.campaign_code,a.offer_desc, EXTRACT(DATE from a.activation_start_date) as activation_start_date, a.offer_end_date, 
 a.offer_type, a.reward_currency_type 
from wx-bq-poc.loyalty.et_targeted_customer_offer a 
where a.campaign_start_date>='2021-06-01' and a.offer_end_date<'2021-12-01' and 
a.offer_desc NOT LIKE '%Bupa%' 
AND a.offer_desc NOT LIKE '%Ampol%' 
AND a.offer_desc NOT LIKE '%PetCulture%';
 




create  or replace table akelly.campaign_test2  as
select a.* from (SELECT
t.crn, t.campaign_code,t.offer_desc, t.offer_type, t.reward_currency_type ,t.activation_start_date,
DATE_DIFF(t.offer_end_date ,t.activation_start_date,day) as RUN_LENGTH,
b.ref_dt, b.pred_bin as pred_bin_before ,
b.adjusted_score_spend as score_spend_before,
case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_before,
ROW_NUMBER() OVER (PARTITION BY b.crn,t.campaign_code ORDER BY b.ref_dt DESC) dest_rank 
    from akelly.campaign_test0  as t join 
     score.cfv_instore_segments as b
     on t.crn=b.crn AND
      b.ref_dt between date_sub(t.activation_start_date, interval 1 month) AND t.activation_start_date
)  a
     where a.dest_rank=1;

 


#################





create  or replace table akelly.campaign_test3  as
select a.* from (SELECT
t.crn, t.campaign_code,t.offer_end_date,b.ref_dt, b.pred_bin as pred_bin_after ,
b.adjusted_score_spend as score_spend_after,case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_after,
ROW_NUMBER() OVER (PARTITION BY b.crn ,t.campaign_code ORDER BY b.ref_dt DESC) dest_rank 
    from akelly.campaign_test0 as t join 
     score.cfv_instore_segments as b
     on t.crn=b.crn AND 
      b.ref_dt between  t.offer_end_date AND DATE_ADD(t.offer_end_date, interval 1 month) 
)  a
     where a.dest_rank=1 ;



create  or replace table akelly.campaign_test3  as
SELECT
t.crn, t.campaign_code,t.offer_end_date,b.ref_dt, b.pred_bin as pred_bin_after ,
b.adjusted_score_spend as score_spend_after,case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_after,
ROW_NUMBER() OVER (PARTITION BY b.crn,t.campaign_code ORDER BY b.ref_dt DESC) dest_rank 
    from akelly.campaign_test0 as t join 
     score.cfv_instore_segments as b
     on t.crn=b.crn AND 
      b.ref_dt between  t.offer_end_date AND DATE_ADD(t.offer_end_date, interval 1 month) 
;








create  or replace table akelly.campaign_test4  as
  SELECT b.crn,b.offer_desc,b.offer_type,b.reward_currency_type,b.campaign_code,b.RUN_LENGTH,
        CASE when c.pred_rank_after>b.pred_rank_before then 1 else 0 end as cav_conversion,
        CASE when c.score_spend_after>b.score_spend_before then 1 else 0 end as spend_increase_conversion,
        CASE when c.score_spend_after>b.score_spend_before*1.05 then 1 else 0 end as spend_increase_conversion_5pcent,
        CASE when c.score_spend_after>b.score_spend_before*1.1 then 1 else 0 end as spend_increase_conversion_10pcent,
        (c.score_spend_after/b.score_spend_before)-1 as spend_change_pcent,
        b.score_spend_before,c.score_spend_after,
        b.pred_bin_before,c.pred_bin_after
        from 
        akelly.campaign_test2 as b left  join 
        akelly.campaign_test3 as c on 
        (b.crn=c.crn and b.campaign_code=c.campaign_code )
        ;



create  or replace table akelly.campaign_newbin as  
select * EXCEPT (pred_bin_after,cav_conversion), 
case when pred_bin_before='high' and pred_bin_after='high' and score_spend_after-score_spend_before>=2000 then 'extra high' else
pred_bin_after end as pred_bin_after,
case when pred_bin_before='high' and pred_bin_after='high' and score_spend_after-score_spend_before>=2000 then 1 else
cav_conversion end as cav_conversion
from  akelly.campaign_test4 ; 

 
create  or replace table akelly.campaign_test5  as select 
aa.*  
,count(case when b.activation_ts IS NOT NULL and a.test_exec_ind != 'Y' then 1  end) as activated
FROM akelly.campaign_newbin  as aa INNER JOIN
        wx-bq-poc.loyalty.campaign_exec a on aa.campaign_code=a.cmpgn_code
INNER JOIN  wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation b 
on          a.cmpgn_exec_id = b.cmpgn_exec_id  and aa.crn=b.crn
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;



create  or replace table akelly.campaign_spend_analysis_cav  as
select offer_desc,
case when activated!=0 then 1 else 0 end as activated_status
,pred_bin_before
,pred_bin_after
, count(*) as cnt
,avg(score_spend_before) as avg_score_spend_before
,avg(score_spend_after) as avg_score_spend_after 
from akelly.campaign_test5 
group by 1,2,3,4;


create  or replace table akelly.campaign_spend_analysis  as
select offer_desc,case when activated!=0 then 1 else 0 end as activated_status, 
avg(score_spend_before) as avg_score_spend_before, avg(score_spend_after) as avg_score_spend_after from akelly.campaign_test5 group by 1,2;

 

 

create  or replace table akelly.campaign_test6 as
select   
b.offer_desc,b.offer_type
,b.reward_currency_type
,AVG(b.RUN_LENGTH) as avg_run_length
, count(distinct(b.campaign_code)) as campaigns
,count(*) as customers
, count(distinct(b.crn)) as unique_customers
, count( case when b.activated !=0 then 1 end) as no_of_activated
, count( case when b.pred_bin_after is NULL then 1 end)/count(*)  as campaign_churn
, sum(b.cav_conversion) as cav_conversion_sum
, sum(case when b.activated !=0 then b.cav_conversion end) as cav_conversion_activated_sum
, sum(b.cav_conversion)/count(*) as cav_conversion_rate
, sum(case when b.activated !=0 then b.cav_conversion end)/count(*) as cav_conversion_activated_rate
, sum(case when b.activated !=0 then b.cav_conversion end)/count( case when b.activated !=0 then 1 end) as cav_conversion_activated_rate_from_activated

, sum(  b.spend_increase_conversion) as spend_increase_conversion_sum
, sum(case when b.activated !=0 then b.spend_increase_conversion end) as spend_increase_conversion_activated_sum

, sum( b.spend_increase_conversion)/count(*) as spend_increase_conversion_rate
, sum(case when b.activated !=0 then  b.spend_increase_conversion end)/count(*) as spend_increase_conversion_activated_rate


, sum( b.spend_increase_conversion_5pcent) as spend_increase_conversion_5pcent_sum
, sum(case when b.activated !=0 then   b.spend_increase_conversion_5pcent end) as spend_increase_conversion_activated_5pcent_sum


, sum(b.spend_increase_conversion_5pcent)/count(*) as spend_increase_conversion_5pcent_rate
, sum(case when b.activated !=0 then  b.spend_increase_conversion_5pcent end)/count(*) as spend_increase_conversion_activated_5pcent_rate


, sum( b.spend_increase_conversion_10pcent) as spend_increase_conversion_10pcent_sum
, sum(case when b.activated !=0 then   b.spend_increase_conversion_10pcent end) as spend_increase_conversion_activated_10pcent_sum


, sum(b.spend_increase_conversion_10pcent)/count(*) as spend_increase_conversion_10pcent_rate
, sum(case when b.activated !=0 then  b.spend_increase_conversion_10pcent end)/count(*) as spend_increase_conversion_activated_10pcent_rate
,AVG(case when b.activated =0 then   b.spend_change_pcent end) as after_spend_pcent_not_activated_change
,AVG(case when b.activated !=0 then   b.spend_change_pcent end) as after_spend_pcent_activated_change
, AVG(case when b.activated !=0 then  b.score_spend_before end) as avg_spend_before_activated
, AVG(case when b.activated !=0 then  b.score_spend_after end) as avg_spend_after_activated

, AVG(case when b.activated !=0 and cav_conversion>0 then  b.score_spend_before end) as avg_spend_before_uplifted
, AVG(case when b.activated !=0 and cav_conversion>0 then  b.score_spend_after end) as avg_spend_after_uplifted

,AVG( b.spend_change_pcent) as after_spend_pcent_change
, AVG(b.score_spend_before) as avg_spend_before
, AVG(b.score_spend_after) as avg_spend_after
,STDDEV(case when activated !=0 then 1 else 0 end) as activated_std
,STDDEV(case when activated !=0 and cav_conversion=1 then 1 else 0 end) as activated_cav_conversion_std
,STDDEV(case when activated !=0 and spend_increase_conversion_5pcent=1 then 1 else 0 end) as activated_spend_increase_conversion_5pcent_std
,STDDEV(case when activated !=0 and spend_increase_conversion_10pcent=1 then 1 else 0 end) as activated_spend_increase_conversion_10pcent_std
from akelly.campaign_test5 b group by 1,2,3;


#max ref dt 2021-01-17

create or replace table akelly.CAV_churn_2019_2020 as
WITH yrone as (
select crn, case
when supers_tot_spend <=1930.0 then "Low"
when supers_tot_spend>1930.0 and supers_tot_spend<=3977.0 then 'Low Medium'
when supers_tot_spend>3977.0 and supers_tot_spend<=5955.0 then 'Medium'
when supers_tot_spend>5955.0 and supers_tot_spend<=8194.0 then 'High Medium'
when supers_tot_spend >8194.0 then "High"
end as supers_tot_spend_bin,
case
when supers_tot_spend <=1930.0 then 1
when supers_tot_spend>1930.0 and supers_tot_spend<=3977.0 then 2
when supers_tot_spend>3977.0 and supers_tot_spend<=5955.0 then 3
when supers_tot_spend>5955.0 and supers_tot_spend<=8194.0 then 4
when supers_tot_spend >8194.0 then 5
end as bin_rank,
from supers.supers_target
where supers_tot_spend>0 and ref_dt>='2019-11-01'and ref_dt<'2019-12-01'


),
 yrtwo as (
select b.crn, case
when b.supers_tot_spend <=0 then "zero"
when b.supers_tot_spend <=1930.0 then "Low"
when b.supers_tot_spend>1930.0 and b.supers_tot_spend<=3977.0 then 'Low Medium'
when b.supers_tot_spend>3977.0 and b.supers_tot_spend<=5955.0 then 'Medium'
when b.supers_tot_spend>5955.0 and b.supers_tot_spend<=8194.0 then 'High Medium'
when b.supers_tot_spend >8194.0 then "High"
end as supers_tot_spend_bin,
case
when b.supers_tot_spend <=0 then 0
when b.supers_tot_spend <=1930.0 then 1
when b.supers_tot_spend>1930.0 and b.supers_tot_spend<=3977.0 then 2
when b.supers_tot_spend>3977.0 and b.supers_tot_spend<=5955.0 then 3
when b.supers_tot_spend>5955.0 and b.supers_tot_spend<=8194.0 then 4
when b.supers_tot_spend >8194.0 then 5
end as bin_rank,
from  supers.supers_target as b
where  b.ref_dt>='2020-11-01'and b.ref_dt<'2020-12-01'

),


joincust as (

select DISTINCT a.crn
,a.supers_tot_spend_bin as supers_tot_spend_bin_1
,b.supers_tot_spend_bin as supers_tot_spend_bin_2
,a.bin_rank as bin_rank_1
,b.bin_rank as bin_rank_2
from yrone as a left join yrtwo as b on a.crn=b.crn
group by 1,2,3,4,5
)

select supers_tot_spend_bin_1,bin_rank_1, supers_tot_spend_bin_2,bin_rank_2,count(*) as cnt, count(distinct crn) as customers 
from joincust group by 1,2,3,4; 
 



create or replace table akelly.CAV_churn_2019_2020_supers_instore as
select 
case when supers_tot_spend_bin_1!='zero' and supers_tot_spend_bin_1='zero' then   'churn_to_zero'
when supers_tot_spend_bin_1!='zero' and supers_tot_spend_bin_2 is null then  'churned'
when bin_rank_1< bin_rank_2 then  'churn_to_lower_CAV'
when bin_rank_1= bin_rank_2 then  'same_CAV'
else 'other' end as churn_analysis
,sum(cnt) as cnt
,sum(customers) as customers

from akelly.CAV_churn_2019_2020
group by 1;





create table akelly.dec_jan_cav_compare as 

WITH yrone as (
select crn, pred_bin,
case
when pred_bin ='zero'  then 0
when pred_bin ='low'  then 1
when pred_bin ='lowmed'  then 2
when pred_bin ='med'  then 3
when pred_bin ='highmed'  then 4
when pred_bin ='high'  then 5
end as bin_rank,
from vw_score.cfv_instore_segments
where ref_dt='2021-12-05'

),
 yrtwo as (
select crn,pred_bin,
case
when pred_bin ='zero'  then 0
when pred_bin ='low'  then 1
when pred_bin ='lowmed'  then 2
when pred_bin ='med'  then 3
when pred_bin ='highmed'  then 4
when pred_bin ='high'  then 5
end as bin_rank,
from  vw_score.cfv_instore_segments as b
where  b.ref_dt='2022-01-02'

),

joincust as (

select a.crn
,a.pred_bin as pred_bin_1
,b.pred_bin as pred_bin_2
,a.bin_rank as bin_rank_1
,b.bin_rank as bin_rank_2
from yrone as a left join yrtwo as b on a.crn=b.crn
)

select pred_bin_1,bin_rank_1, pred_bin_2,bin_rank_2,count(*) as cnt, count(distinct crn) as customers 
from joincust group by 1,2,3,4; 



select pred_bin_1,bin_rank_1, pred_bin_2,bin_rank_2,count(*) as cnt, count(distinct crn) as customers 
from joincust group by 1,2,3,4; 


select * from supers.supers_target where crn='3300000000005134326';



select b.crn, case
when b.supers_tot_spend <=0 then "zero"
when b.supers_tot_spend <=1930.0 then "Low"
when b.supers_tot_spend>1930.0 and b.supers_tot_spend<=3977.0 then 'Low Medium'
when b.supers_tot_spend>3977.0 and b.supers_tot_spend<=5955.0 then 'Medium'
when b.supers_tot_spend>5955.0 and b.supers_tot_spend<=8194.0 then 'High Medium'
when b.supers_tot_spend >8194.0 then "High"
end as supers_tot_spend_bin,
case
when b.supers_tot_spend <=0 then 0
when b.supers_tot_spend <=1930.0 then 1
when b.supers_tot_spend>1930.0 and b.supers_tot_spend<=3977.0 then 2
when b.supers_tot_spend>3977.0 and b.supers_tot_spend<=5955.0 then 3
when b.supers_tot_spend>5955.0 and b.supers_tot_spend<=8194.0 then 4
when b.supers_tot_spend >8194.0 then 5
end as bin_rank,
from   join supers.supers_target as b  
where  b.ref_dt>='2021-11-01'and b.ref_dt<'2021-12-01' and crn='1100000000001511507';


1100000000001511507

1100000000094392463
Low
1
3   
1100000000001631270
Low
1
4   
1100000000001469303
Low
1
5   
1100000000103470904
Low




CREATE OR REPLACE TABLE
WITH campaign as 
(
select
   b.PrimaryCustomerRegistrationNumber as crn ,a.campaign_code,a.offer_desc, EXTRACT(DATE from a.activation_start_date) as activation_start_date, a.offer_end_date, 
 a.offer_type, a.reward_currency_type 
from wx-bq-poc.loyalty.et_targeted_customer_offer a 
where a.campaign_start_date>='2021-01-01' and a.offer_end_date<'2021-12-01'   limit 20

),



PRED_PREV as (
select a.* from (SELECT
t.crn, t.campaign_code,t.offer_desc, t.offer_type, t.reward_currency_type ,t.activation_start_date,b.ref_dt, b.pred_bin as pred_bin_before ,
b.score_spend as score_spend_before,
case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_before,
ROW_NUMBER() OVER (PARTITION BY b.crn ORDER BY b.ref_dt DESC) dest_rank 
    from campaign as t join 
     score.cfv_instore_segments as b
     on t.crn=b.crn AND
      b.ref_dt between date_sub(t.activation_start_date, interval 1 month) AND t.activation_start_date
)  a
     where a.dest_rank=1

),

PRED_AFTER as (
select a.* from (SELECT
t.crn, t.campaign_code,t.offer_end_date,b.ref_dt, b.pred_bin as pred_bin_after ,
b.score_spend as score_spend_after,case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_after,
ROW_NUMBER() OVER (PARTITION BY b.crn ORDER BY b.ref_dt DESC) dest_rank 
    from campaign as t join 
     score.cfv_instore_segments as b
     on t.crn=b.crn AND 
      b.ref_dt between  t.offer_end_date AND DATE_ADD(t.offer_end_date, interval 1 month) 
)  a
     where a.dest_rank=1),


COMBINE as (
select b.*,c.pred_bin_after , c.pred_rank_after ,c.score_spend_after, c.offer_end_date
DATE_DIFF(c.offer_end_date ,b.activation_start_date,day) as RUN_LENGTH,
CASE when c.pred_rank_after>b.pred_rank_before then 1 else 0 end as cav_conversion,
CASE when c.score_spend_after>b.score_spend_before then 1 else 0 end as spend_increase_conversion
from 
PRED_PREV as b on  join 
PRED_AFTER as c on 
(b.crn=c.crn and b.campaign_code=c.campaign_code )
)

SELECT offer_desc,offer_type,reward_currency_type, count(distinct(campaign_code)) as campaigns, count(*) as cnt, sum(cav_conversion) as cav_conversion_sum,
sum(spend_increase_conversion) as spend_increase_conversion_sum
from COMBINE group by 1,2,3
;




create table akelly.campaignanalysis as 
with base as (
select cast(crn as string) as crn ,"control" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn not in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
union DISTINCT
select cast(crn as string) as crn ,"test" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn in (select cast(crn as string) as crn from wx-bq-poc.loyalty_campaign_analytics.rtn4879_final where pred_bin = 'hvcfv' )
and cast(crn as string) in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879' )


UNION DISTINCT 
 
SELECT b.crn, "test1" as aud_type 
    from `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events` as a 
    join wx-bq-poc.adh_own_data.redx_loyalty_customer_encrypted_detail as b on
    a.udo_user_profile_crn_hash=b.crn_enc
    where 
    (udo_campaign_offer_id='-218616' or udo_campaign_code='RTN-4879' )
     and udo_page_name like 'ww-rw:offers%'
     and udo_tealium_event = 'rw_offer_activation_success'
)

,sent as (
select distinct crn,1 as send_ind 
from wx-bq-poc.loyalty.et_targeted_customer_offer
where campaign_code= 'RTN-4879'
)

,activation as (
Select distinct(cecoa.crn),1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        wx-bq-poc.loyalty.campaign_exec ce
INNER JOIN  wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation cecoa 
on          ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and         cecoa.offer_id IN (-218616,-216217) 
WHERE       cecoa.activation_ts IS NOT NULL 
AND         ce.cmpgn_code = 'RTN-4879'  
AND         ce.test_exec_ind != 'Y'
)


,temp_base_aud as (
       select bse.crn,aud_type,
       case when send_ind=1 then 1 else 0 end as send_ind
      ,case when act_ind=1 or aud_type='test2' then 1 else 0 end as act_ind
from base as bse
left join sent 
on cast(bse.crn as string)= sent.crn
left join activation as act 
on cast(bse.crn as string)=act.crn
)


,add_loyalty as ( 
select b.PrimaryCustomerRegistrationNumber as crn, b.LoyaltyCardNumber,min(LoyaltyCardRegistrationDate) as lylty_card_rgstr_date 
from `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v b
WHERE b.PrimaryCustomerRegistrationNumber in (select crn from temp_base_aud)
group by 1,2)

select a.*,
case 
when a.act_ind  =1 then 'accepted'
when a.aud_type='test' and a.send_ind=1 then 'ignored'
else 'control' end as cust_type
,c.LoyaltyCardNumber
from temp_base_aud as a 
left join add_loyalty as c on a.crn=c.crn






#calc avg. 
select avg(adjusted_score_spend)
from score.cfv_instore_segments
where pred_bin='high' and ref_dt='2020-02-06';









#python scrap code



pd.read_gbq("select * from score.cfv_instore_segments where ref_dt='2022-01-02' and pred_bin='zero' limit 4", project_id=project_id)

pd.read_gbq('select count(*) from akelly.campaign_test6', project_id=project_id)
campaigns =pd.read_gbq('select * from akelly.campaign_test6 order by 1', project_id=project_id)


pd.read_gbq("""select * from akelly.CAV_thresholds_jan2021jan2022;"""
, project_id=project_id)

pd.read_gbq("""select distinct campaign_code,activation_start_date,offer_end_date,
DATE_DIFF(offer_end_date ,activation_start_date,day) as RUN_LENGTH from akelly.campaign_test1 where  offer_desc='Spend and Get Points' limit 5  """)



q="""select * from akelly.campaign_test2 where offer_desc='10x points on Ready Meals'"""
pd.read_gbq(q, project_id=project_id)



q="""select * from akelly.campaign_test3 where campaign_code='CAT-4959'"""
pd.read_gbq(q, project_id=project_id)


q="""select * from akelly.campaign_test4 where campaign_code='CAT-4959' limit 100"""
d=pd.read_gbq(q, project_id=project_id)


q="""select * from akelly.campaign_test3 where campaign_code='CAT-4959' and crn='3300000000002319872'"""
pd.read_gbq(q, project_id=project_id)
q="""select * from akelly.campaign_test1 where campaign_code='CAT-4959' and crn='3300000000002319872'"""
pd.read_gbq(q, project_id=project_id)
q="""select * from akelly.campaign_test2 where campaign_code='CAT-4959' and crn='3300000000002319872'"""
pd.read_gbq(q, project_id=project_id)



q="""SELECT
t.crn, t.campaign_code,t.offer_end_date,b.ref_dt, b.pred_bin as pred_bin_after ,
b.adjusted_score_spend as score_spend_after,case
when b.pred_bin ='zero' then 0
when b.pred_bin ='low' then 1
when b.pred_bin ='lowmed' then 2
when b.pred_bin ='med' then 3
when b.pred_bin ='highmed' then 4
when b.pred_bin ='high' then 5
else 0 end as pred_rank_after,
ROW_NUMBER() OVER (PARTITION BY b.crn ORDER BY b.ref_dt DESC) dest_rank 
    from akelly.campaign_test0 as t join 
     score.cfv_instore_segments as b
     on t.crn=b.crn AND 
      b.ref_dt between  t.offer_end_date AND DATE_ADD(t.offer_end_date, interval 1 month) 
    where t.crn='3300000000002319872' and  t.campaign_code='CAT-4959'"""



q="""select DATE_ADD('2021-09-22', interval 1 month) as after,* from score.cfv_instore_segments where  ref_dt between  '2021-09-22' AND DATE_ADD('2021-09-22', interval 1 month) limit 5 """
d=pd.read_gbq(q, project_id=project_id)


q="""select * from score.cfv_instore_segments where  crn='3300000000002319872' order by ref_dt desc limit 5 """
pd.read_gbq(q, project_id=project_id)


q="""
select * from `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v where 
PrimaryCustomerRegistrationNumber='3300000000003024753'  """
pd.read_gbq(q, project_id=project_id)


q="""select STDDEV(cav_conversion_rate) from akelly.campaign_test5"""
pd.read_gbq(q, project_id=project_id)



q="""select * from wx-bq-poc.loyalty.et_targeted_customer_offer where campaign_code='CAT-4959'
order by 1 limit 10; """
pd.read_gbq(q, project_id=project_id)


q="""select * from  akelly.campaign_test4
order by 1 limit 10; """
pd.read_gbq(q, project_id=project_id)

q="""
select count(distinct(crn)), 
count(case when b.activation_ts IS NOT NULL then 1 end) as activated
FROM        wx-bq-poc.loyalty.campaign_exec a
INNER JOIN  wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation b 
on          a.cmpgn_exec_id = b.cmpgn_exec_id
 where a.cmpgn_code='CAT-4959'"""
pd.read_gbq(q, project_id=project_id)



q="""select user_activn_ind, count(*) from wx-bq-poc.loyalty.et_targeted_customer_offer where campaign_code='CAT-4959' group by 1 """
pd.read_gbq(q, project_id=project_id)


q="""select count(*) from akelly.campaign_test0 where activation_start_date is NULL """
pd.read_gbq(q, project_id=project_id)

q="select * from score.cfv_instore_segments limit 5"
d0=pd.read_gbq(q, project_id=project_id)

q="""select * from akelly.CAV_churn_2019_2020 limit 20 """
d1=pd.read_gbq(q, project_id=project_id)

q="""select * from akelly.CAV_churn_2019_2020_supers_instore  """
d=pd.read_gbq(q, project_id=project_id)




 #Value of the cohort in today's dollars is sum of PVs
cohort_uplifted_value_tot = sum(spending_after_uplifted)
cohort_uplifted_value = sum(spending_after_uplifted)-sum(spending_before_uplifted)
spread=int(cohort_uplifted_value - CAC)
print('Total Cohort Value for CAV converted: ', int(cohort_uplifted_value_tot))
print('Total additional spend for CAV converted: ', int(cohort_uplifted_value))
print('CLTV-CAC Spread: ', int(cohort_uplifted_value - CAC))





chosen_campaign.avg_spend_before
chosen_campaign.avg_spend_after
chosen_campaign.avg_spend_before_activated
chosen_campaign.avg_spend_after_activated

chosen_campaign.after_spend_pcent_change
chosen_campaign.after_spend_pcent_activated_change
chosen_campaign.campaign_churn



# Run the function
customers_activated_before, spending_before_activated =simulate_cohort(cohort_size_activated[0], chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_activated.iloc[0], yrs=1,before=True)

customers_activated_after, spending_after_activated =simulate_cohort(cohort_size_activated[0], chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_activated.iloc[0], yrs=1)


customers_uplifted_before, spending_before_uplifted =simulate_cohort(cohort_size_uplift[0], chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_uplifted.iloc[0], yrs=1,before=True)

customers_uplifted_after, spending_after_uplifted =simulate_cohort(cohort_size_uplift[0], chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_uplifted.iloc[0], yrs=1)


# Run the function
activated_conversion_rate = get_conversion_rate(conversion_rate_activated_expected, 
                                      conversion_rate_activated_std)


activated_uplift_conversion_rate = get_conversion_rate(conversion_rate_uplifted_expected, 
                                      conversion_rate_uplifted_std)

cohort_size_activated = run_campaign( activated_conversion_rate,targeted_customers=targeted_customers)

cohort_size_uplift = run_campaign( activated_uplift_conversion_rate,targeted_customers=cohort_size_activated)
#CAC = spend/cohort_size
#cost of campaign
CAC = int(activated_cost*cohort_size_activated)
Pcent_uplift=cohort_size_uplift[0]/cohort_size_activated
Pcent_activated=cohort_size_activated[0]/targeted_customers



print('Customers activated: ',  f'{int(cohort_size_activated):,}')
print('Customers CAV uplifted: ',  f'{int(cohort_size_uplift):,}')
print('CAV activation cost: $', f'{int(CAC):,}' )

print('Customers activated: ',  f'{Pcent_activated.iloc[0]:.01%}')
print('Customers CAV uplifted: ',  f'{Pcent_uplift[0]:.01%}')





 #Value of the cohort in today's dollars is sum of PVs
cohort_uplifted_value_tot = sum(spending_after_uplifted)
cohort_uplifted_value = sum(spending_after_uplifted)-sum(spending_before_uplifted)
spread=int(cohort_uplifted_value - CAC)
print('Total Cohort Value for CAV converted: ', int(cohort_uplifted_value_tot))
print('Total additional spend for CAV converted: ', int(cohort_uplifted_value))
print('CLTV-CAC Spread: ', int(cohort_uplifted_value - CAC))



simulate=pd.DataFrame(columns=['CAC','cohort_size_activated','cohort_size_uplift','Pcent_uplift','Pcent_activated','customers_activated_before','customers_activated_after','customers_uplifted_before','customers_uplifted_after','spending_before_activated', 'spending_after_activated', 'spending_before_uplifted', 'spending_after_uplifted','cohort_uplifted_value','spread' ])



# Simulate 1000 times and look at the distributions
activated_cost=5
# Conversion rate

chosen_campaign=campaigns[campaigns.offer_desc=='10x points on the entire Macro range']
targeted_customers=chosen_campaign.customers/chosen_campaign.campaigns
conversion_rate_activated_expected =chosen_campaign.cav_conversion_rate.iloc[0]
conversion_rate_activated_std=chosen_campaign.activated_std.iloc[0]
conversion_rate_uplifted_expected =chosen_campaign.cav_conversion_activated_rate_from_activated.iloc[0]
conversion_rate_uplifted_std=chosen_campaign.activated_cav_conversion_std.iloc[0]




for i in range(1000):
    
    # Run marketing campaign sim
    activated_conversion_rate = get_conversion_rate(conversion_rate_activated_expected,conversion_rate_activated_std)
    activated_uplift_conversion_rate = get_conversion_rate(conversion_rate_uplifted_expected, conversion_rate_uplifted_std)
    cohort_size_activated = run_campaign( activated_conversion_rate,targeted_customers=targeted_customers)
    cohort_size_uplift = run_campaign( activated_uplift_conversion_rate,targeted_customers=cohort_size_activated)
    #CAC = spend/cohort_size
    #cost of campaign
    CAC = int(activated_cost*cohort_size_activated)
    Pcent_uplift=cohort_size_uplift[0]/cohort_size_activated
    Pcent_activated=cohort_size_activated[0]/targeted_customers
    # Run the function
    customers_activated_before, spending_before_activated =simulate_cohort(cohort_size_activated[0], chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_activated.iloc[0], yrs=1,before=True)

    customers_activated_after, spending_after_activated =simulate_cohort(cohort_size_activated[0], chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_activated.iloc[0], yrs=1)


    customers_uplifted_before, spending_before_uplifted =simulate_cohort(cohort_size_uplift[0], chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_uplifted.iloc[0], yrs=1,before=True)

    customers_uplifted_after, spending_after_uplifted =simulate_cohort(cohort_size_uplift[0], chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_uplifted.iloc[0], yrs=1)


    temp=pd.DataFrame([[CAC,cohort_size_activated[0],cohort_size_uplift[0],Pcent_uplift[0],\
                               float(Pcent_activated),customers_activated_before,customers_activated_after,\
                                   customers_uplifted_before,customers_uplifted_after,spending_before_activated[0],\
                                   spending_after_activated[0], spending_before_uplifted[0],spending_after_uplifted[0],\
                                   cohort_uplifted_value,spread]],columns=['CAC','cohort_size_activated','cohort_size_uplift',\
                                                                          'Pcent_uplift','Pcent_activated','customers_activated_before',\
                                                                          'customers_activated_after','customers_uplifted_before',\
                                                                          'customers_uplifted_after','spending_before_activated',\
                                                                          'spending_after_activated', 'spending_before_uplifted',\
                                                                          'spending_after_uplifted','cohort_uplifted_value','spread' ])


    simulate=simulate.append(temp)
   
   
   
simulate.head()
# Store simulation results in a dataframe
simulate=simulate.reset_index()
 
simulate.to_feather('results_df.feather')
simulate=pd.read_feather('results_df.feather')
plot_df = simulate #[results_df['CAC']<=1000]


# Histogram for distribution of initial cohort size
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['customers_uplifted_after'],color='green' ,kde=False, bins=100)

ax.set_xlabel("Initial Cohort Size",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.title('Estimated customers with increased CAV', fontsize=20)
plt.tight_layout()
plt.ticklabel_format(style='plain', axis='x')
plt.savefig(fname='cohort_hist', dpi=150)
plt.show()







